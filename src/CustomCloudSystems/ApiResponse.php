<?php

namespace CustomCloudSystems;

/**
 * Class ApiResponse
 * @package CustomCloudSystems
 * @author Custom Cloud Systems LLC
 * Application API response object
 *
 */
class ApiResponse
{
    const STATUS_INTERNAL_ERROR = 500; // keep for legacy
    const STATUS_UNAVAILABLE    = 503; // keep for legacy

    // 100 codes
    //
    const STATUS_CONTINUE            = 100;
    const STATUS_SWITCHING_PROTOCOLS = 101;
    const STATUS_PROCESSING          = 102;

    // 200 Codes
    //
    const STATUS_OK               = 200;
    const STATUS_CREATED          = 201;
    const STATUS_ACCESS           = 202;
    const STATUS_NON_AUTH_INFO    = 203;
    const STATUS_NO_CONTENT       = 204;
    const STATUS_RESET_CONTENT    = 205;
    const STATUS_PARTIAL_CONTENT  = 206;
    const STATUS_MULTI_STATUS     = 207;
    const STATUS_ALREADY_REPORTED = 208;
    const STATUS_IM_USED          = 226;

    // 300 Codes
    //
    const STATUS_MULTI_CHOICES      = 300;
    const STATUS_MOVED_PERMANENT    = 301;
    const STATUS_FOUND              = 302;
    const STATUS_SEE_OTHER          = 303;
    const STATUS_NOT_MODIFIED       = 304;
    const STATUS_USE_PROXY          = 305;
    const STATUS_UNUSED             = 306;
    const STATUS_TEMPORARY_REDIRECT = 307;
    const STATUS_PERMANENT_REDIRECT = 308;

    // 400 Codes
    //
    const STATUS_BAD_REQUEST                     = 400;
    const STATUS_UNAUTHORIZED                    = 401;
    const STATUS_PAYMENT_REQUIRED                = 402;
    const STATUS_FORBIDDEN                       = 403;
    const STATUS_NOT_FOUND                       = 404;
    const STATUS_METHOD_NOT_ALLOWED              = 405;
    const STATUS_NOT_ACCEPTABLE                  = 406;
    const STATUS_PROXY_AUTHENTICATION_REQUIRED   = 407;
    const STATUS_REQUEST_TIMEOUT                 = 408;
    const STATUS_CONFLICT                        = 409;
    const STATUS_GONE                            = 410;
    const STATUS_LENGTH_REQUIRED                 = 411;
    const STATUS_PRECONDITION_FAILED             = 412;
    const STATUS_REQUEST_ENTITY_TOO_LARGE        = 413;
    const STATUS_REQUEST_URI_TOO_LONG            = 414;
    const STATUS_UNSUPPORTED_MEDIA_TYPE          = 415;
    const STATUS_REQUEST_RANGE_NOT_SATISFIABLE   = 416;
    const STATUS_EXPECTATION_FAILED              = 417;
    const STATUS_IM_A_TEAPOT                     = 418;
    const STATUS_ENHANCE_YOUR_CALM               = 420;
    const STATUS_UNPROCESSABLE_ENTITY            = 422;
    const STATUS_LOCKED                          = 423;
    const STATUS_FAILED_DEPENDENCY               = 424;
    const STATUS_RESERVED_FOR_WEBDAV             = 425;
    const STATUS_UPGRADE_REQUIRED                = 426;
    const STATUS_PRECONDITION_REQUIRED           = 428;
    const STATUS_TOO_MANY_REQUESTS               = 429;
    const STATUS_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;
    const STATUS_NO_RESPONSE                     = 444;
    const STATUS_RETRY_WITH                      = 449;
    const STATUS_BLOCKED_BY_WINDOWS_PARENTAL     = 450;
    const STATUS_UNAVAILABLE_FOR_LEGAL_REASONS   = 451;
    const STATUS_CLIENT_CLOSED_REQUEST           = 499;

    // 500 Codes
    //
    const STATUS_INTERNAL_SERVER_ERROR            = 500;
    const STATUS_NOT_IMPLEMENTED                  = 501;
    const STATUS_BAD_GATEWAY                      = 502;
    const STATUS_SERVICE_UNAVAILABLE              = 503;
    const STATUS_GATEWAY_TIMEOUT                  = 504;
    const STATUS_HTTP_VERSION_NOT_SUPPORTED       = 505;
    const STATUS_VARIANT_ALSO_NEGOTIATES          = 506;
    const STATUS_INSUFFICIENT_STORAGE             = 507;
    const STATUS_LOOP_DETECTED                    = 508;
    const STATUS_BANDWIDTH_LIMIT_EXCEEDED         = 509;
    const STATUS_NOT_EXTENDED                     = 510;
    const STATUS_NETWORK_AUTHENTICATION_REQUIRED  = 511;
    const STATUS_NETWORK_READ_TIMEOUT_ERROR       = 598;
    const STATUS_NETWORK_CONNECTION_TIMEOUT_ERROR = 599;

    /**
     * @var string $requestId
     */
    private $requestId;

    /**
     * @var array $data
     */
    private $data;

    /**
     * @var int $statusCode
     */
    private $statusCode;

    /**
     * @var int $systemCode
     */
    private $systemCode;

    /**
     * @var boolean $success
     */
    private $success;

    /**
     * @var array $generatedResponse
     */
    private $generatedResponse;

    /**
     * @var string $rawResponse
     */
    private $rawResponse;

    /**
     * @var string $message
     */
    private $message;

    /**
     * Return an instance of the ApiResponse
     * @return ApiResponse
     */
    public static function create()
    {
        return new ApiResponse;
    }

    /**
     * Return an instance of the ApiResponse and make it a success response
     * @return ApiResponse
     */
    public static function success()
    {
        return self::create()
            ->setSuccess(true);
    }

    /**
     * Return an instance of the ApiResponse and make it an error response
     * @return ApiResponse
     */
    public static function error()
    {
        return self::create()
            ->setSuccess(false);
    }

    /**
     * @return bool
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param boolean $success
     * @return ApiResponse
     */
    public function setSuccess($success = true)
    {
        $this->success = (bool) $success;

        if ($success) {
            $this->statusCode = self::STATUS_OK;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param mixed $requestId
     * @return ApiResponse
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return ApiResponse
     */
    public function setData($data)
    {
        $this->data = (array) $data;
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return ApiResponse
     */
    public function appendToData($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ApiResponse
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ApiResponse
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getSystemCode()
    {
        return $this->systemCode;
    }

    /**
     * @param int $systemCode
     * @return ApiResponse
     */
    public function setSystemCode($systemCode)
    {
        $this->systemCode = $systemCode;
        return $this;
    }

    /**
     * Prepare the API response
     * @return $this
     */
    public function generateResponse()
    {
        if (!$this->requestId) {
            $this->setRequestId(sha1(microtime(true)));
        }

        $this->generatedResponse = [
            'status'      => ($this->success ? 'SUCCESS' : 'ERROR'),
            'status_code' => $this->statusCode,
            'system_code' => $this->systemCode,
            'request_id'  => $this->requestId,
            'message'     => $this->message,
            'data'        => (array) $this->data,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getGeneratedResponse()
    {
        return $this->generatedResponse;
    }

    /**
     * @return string
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * The raw json response
     * @return string
     */
    public function toJson()
    {
        return $this->toJsonOutput();
    }

    /**
     * @return string
     */
    public function toJsonOutput()
    {
        if (is_null($this->requestId)) {
            $this->setRequestId(sha1(microtime(true)));
        }

        $this->generatedResponse = [
            'success'     => $this->success,
            'status'      => ($this->success ? 'SUCCESS' : 'ERROR'),
            'status_code' => $this->statusCode,
            'system_code' => $this->systemCode,
            'request_id'  => $this->requestId,
            'message'     => $this->message,
            'data'        => (array) $this->data,
        ];
        $this->rawResponse = (string) json_encode($this->generatedResponse);

        return $this->rawResponse;
    }

    public function echoJson()
    {
        echo $this->toJson();
    }
}
